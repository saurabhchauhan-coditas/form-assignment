export interface IShowFormProps {
  handleClick: (index: number) => void
  data: any
}