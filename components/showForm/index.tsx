import React, { useContext, useEffect, useState } from 'react';
import { AppContext } from '@/context/AppContext';
import styles from "./ShowForm.module.scss";
import { IShowFormProps } from './showForm.types';

const ShowForm: React.FC<IShowFormProps> = ({ data, handleClick }) => {
  return (
    <div className={styles.showFormContainer}>
      {
        data?.map((val: any, index: number) => {
          return (
            <div className={styles.wrapper} key={index}>
              <div className={styles.img_area}>
                <div className={styles.inner_area}>
                  <img src="https://t3.ftcdn.net/jpg/05/14/36/48/360_F_514364850_xLOQX6SOY2qcjAIcTowsi3xYvHmhmvs0.jpg" alt="logo" height={100} width={100} />
                </div>
              </div>
              <div>
                <div className={styles.heading1}>{val.firstName}</div>
                <div className={styles.heading}>{val.lastName}</div>
                <div className={styles.buttons}>
                  <button className={styles.button} onClick={() => handleClick(index)}>Delete</button>
                  <button className={styles.button}>Edit</button>
                </div>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

export default ShowForm