"use client"
import Header from '@/components/header';
import Input from '@/components/input';
import Label from '@/components/label';
import Select from '@/components/select';
import { getFormData } from '@/utils/formdata';
import React from 'react';
import styles from "./upload.module.scss";

const Upload = () => {

  const handleForm = (e: any) => {
    e.preventDefault();
    // handleFormSubmit(e);
    // if (email.length === 0 && password.length === 0) {
    //   return
    // }
    const formData = getFormData(e);
    alert(JSON.stringify(formData, null, 2));
  }
  return (
    <>
      <section className={styles.uploadForm}>
        <div className={styles.formContainer}>
          <h1 className={styles.heading}>Enter Patients Data</h1>
          <form onSubmit={handleForm}>
            <div className={styles.formElements}>

              
            </div>
            <button type='submit' className={styles.submitBtn}>Submit</button>
          </form>
        </div>
      </section >
    </>
  )
}

export default Upload