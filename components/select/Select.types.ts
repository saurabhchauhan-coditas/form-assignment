type selectProps = JSX.IntrinsicElements['select'];

export interface ISelectProps extends selectProps {
  name: string
  options: { label: string, value: string }[]
}

export interface IOptionProps {
  label: string
  value: string
}