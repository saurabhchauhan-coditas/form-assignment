import { Vidaloka } from '@next/font/google'
import React from 'react'
import { IOptionProps, ISelectProps } from './select.types';
import styles from "./Select.module.scss";

const Select: React.FC<ISelectProps> = ({ name, options, ...props }) => {
  return (
    <>
      <select name={name} className={styles.select} {...props}>
        {
          options.map((val: IOptionProps, index: number) => {
            return (
              <option className={styles.option} key={index} value={val.value}>{val.label}</option>
            )
          })
        }
      </select>
    </>
  )
}

export default Select