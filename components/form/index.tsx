"use client"
import React, { BaseSyntheticEvent, useContext, useState } from 'react'
import styles from "./personelForm.module.scss";
import useFormValidation from '@/utils/useFormValidation';
import { getFormData } from '@/utils/formdata';
import Input from '../input';
import Select from '@/components/select';
import Label from '@/components/label';
import { AppContext } from '../../context/AppContext';

const initalValue = {
  firstName: "",
  weight: "",
  height: ""
}

const PersonelForm = () => {

  const { appDispatch, appState, router } = useContext(AppContext);
  const [values, setValues] = useState(initalValue);
  const [bmi, setBMI] = useState("");
  const { showError, handleFormChange, handleFormSubmit } = useFormValidation();

  const handleInputFormChange = (e: any) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
    const tempHeight = parseInt(values.height) / 10;
    const tempWeight = parseInt(values.weight);
    setBMI(`${Math.round(tempWeight / (tempHeight * tempHeight))}`);
  }

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
    handleFormChange(e);
  }

  const handleForm = (e: BaseSyntheticEvent) => {
    e.preventDefault();
    handleFormSubmit(e);
    const formDATA = getFormData(e);
    if (values.firstName.length === 0) {
      return
    } else {
      alert(JSON.stringify(formDATA, null, 2));
      appDispatch({
        type: "setFormData",
        payload: formDATA
      })
    }
  }

  return (
    <>
      <section className={styles.personelForm}>
        <div className={styles.formContainer}>
          <h1 className={styles.heading}>Enter Form Details</h1>
          <form onSubmit={handleForm}>
            <div className={styles.formElements}>

              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={'firstName'} type="text" placeholder={'Enter firstname'}
                    handleBlur={handleInputChange}
                  />
                  <span className={[styles.error, !showError.firstName && styles.errorVisibility].join(" ")}>{showError.firstName || <>&nbsp;</>}</span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={'lastName'} type="text" placeholder={'Enter lastname'} handleBlur={handleInputChange} />
                  <span className={[styles.error, !showError.lastName && styles.errorVisibility].join(" ")}>{showError.lastName || <>&nbsp;</>}</span>
                </div>
              </div>

              <div className={styles.inputContainer}>

                <div className={styles.specificElements}>
                  <Input name={'mobileNumber'} type="number" placeholder={'Enter mobile number'} handleBlur={handleInputChange} />
                  <span className={[styles.error, !showError.mobile && styles.errorVisibility].join(" ")}>{showError.mobile || <>&nbsp;</>}</span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={'dob'} type="date" placeholder={'Enter dob'} handleBlur={handleInputChange} />
                  <span><>&nbsp;</></span>
                </div>
              </div>

              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={'weight'} type="text" placeholder={'Enter Weight(kg)'} handleBlur={handleInputChange} handleChange={handleInputFormChange} />
                  <span className={[styles.error, !showError.weight && styles.errorVisibility].join(" ")}>{showError.weight || <>&nbsp;</>}</span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={'height'} type="text" placeholder={'Enter Height(cm)'} handleBlur={handleInputChange} handleChange={handleInputFormChange} />
                  <span className={[styles.error, !showError.height && styles.errorVisibility].join(" ")}>{showError.height || <>&nbsp;</>}</span>
                  {bmi && <span className={[styles.error].join(" ")}>BMI-{bmi || <>&nbsp;</>}</span>}
                </div>
              </div>


              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={'country'} type="text" placeholder={'Enter Country of Origin'} handleBlur={handleInputChange} />
                  <span className={[styles.error, !showError.country && styles.errorVisibility].join(" ")}>{showError.country || <>&nbsp;</>}</span>
                </div>
              </div>

              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={"fatherName"} type="text" placeholder={"Enter Father's Name"} handleBlur={handleInputChange} max={20} />
                  <span className={[styles.error, !showError.fatherName && styles.errorVisibility].join(" ")}>{showError.fatherName || <>&nbsp;</>}</span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={"fatherAge"} type="number" placeholder={"Enter Father's Age"} handleBlur={handleInputChange} max={150} />
                  <span><>&nbsp;</></span>
                </div>
              </div>

              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={"fatherCountry"} type="text" placeholder={"Enter Father's Country of origin"} handleBlur={handleInputChange} max={20} />
                  <span className={[styles.error, !showError.fatherCountry && styles.errorVisibility].join(" ")}>{showError.fatherCountry || <>&nbsp;</>}</span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={"motherName"} type="text" placeholder={"Enter Mother's Name"} handleBlur={handleInputChange} max={20} />
                  <span className={[styles.error, !showError.motherName && styles.errorVisibility].join(" ")}>{showError.motherName || <>&nbsp;</>}</span>
                </div>
              </div>

              <div className={styles.inputContainer}>
                <div className={styles.specificElements}>
                  <Input name={"motherAge"} type="number" placeholder={"Enter Mother's Age"} handleBlur={handleInputChange} />
                  <span><>&nbsp;</></span>
                </div>

                <div className={styles.specificElements}>
                  <Input name={"motherCountry"} type="text" placeholder={"Enter Mother's Country of origin"} handleBlur={handleInputChange} max={20} />
                  <span className={[styles.error, !showError.motherCountry && styles.errorVisibility].join(" ")}>{showError.motherCountry || <>&nbsp;</>}</span>
                </div>
              </div>

              <Label htmlfor="isDiabetic" name='Are you diabetic or pre-diabetic?' />
              <Select name={'isDiabetic'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />

              <Label htmlfor="isCardiac" name='Have you suffered any cardiac related issues in the past or are suffering currently? ' />
              <Select name={'isCardiac'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />

              <Label htmlfor="isBP" name='Do you have concerns with your blood pressure?' />
              <Select name={'isBP'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />

              <Label htmlfor="isDiabetic" name="Is any of your parents diabetic or pre-diabetic?" />
              <Select name={'isDiabetic'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />

              <Label htmlfor="isCardiac" name="Have any of your parents suffered any cardiac related issues in the past or are suffering currently? " />
              <Select name={'isCardiac'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />

              <Label htmlfor="isBP" name="Are any of your parents concerned with their blood pressure?" />
              <Select name={'isBP'} options={[{ label: '', value: '' }, { label: 'Yes', value: 'Yes' }, { label: 'No', value: 'No' }]} />
            </div>

            <div className={styles.inputContainer}>
              <Label htmlfor="aadharFront" name="Upload aadhar card (front)" />
              <Input name={'aadharFront'} type="file" />

              <Label htmlfor="aadharBack" name="Upload aadhar card (back)" />
              <Input name={'aadharBack'} type="file" />
            </div>

            <div className={styles.inputContainer}>
              <Label htmlfor="medicalInsrFront" name='Upload medical insurance card (front' />
              <Input name={'medicalInsrFront'} type="file" />

              <Label htmlfor="medicalInsrBack" name='Upload medical insurance card (back)' />
              <Input name={'medicalInsrBack'} type="file" />
            </div>

            <button type='submit' className={styles.submitBtn}>Submit</button>
            <button className={styles.submitBtn} onClick={() => router.push("/dashboard")}>Dashboard</button>
          </form>
        </div>
      </section>
    </>
  )
}

export default PersonelForm