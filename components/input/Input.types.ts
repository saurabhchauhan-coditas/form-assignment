import { ReactNode } from "react";

type inputProps = JSX.IntrinsicElements['input'];

export interface IInputProps extends inputProps {
  name: string
  placeholder?: string
  handleChange?: (e: any) => void
  handleBlur?: (e: any) => void
}