import React from 'react';
import styles from "./Header.module.scss";

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.logo}><span className={styles.logoSpan}>;</span></div>
      <div className={styles.headingC}><span className={styles.heading}>Connect</span></div>
    </div>
  )
}

export default Header