/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  experimental: {
    appDir: true,
  },
  images: {
    domains: ['images.unsplash.com', "t3.ftcdn.net"],
  },
}

module.exports = nextConfig
