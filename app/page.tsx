"use client"
import React, { useContext } from 'react'
import styles from "./page.module.scss";
import { AppContext } from '@/context/AppContext';
import { useGoogleLogin } from '@react-oauth/google';

const Login = () => {
  const { router } = useContext(AppContext);

  const login = useGoogleLogin({
    onSuccess: (codeResponse: {}) => {
      console.log(codeResponse);
      router.push("/dashboard");
    },
    onError: () => console.log('Login Failed')
  });

  return (
    <section className={styles.login}>
      <div className={styles.sectionLeft}>
        <div className={styles.loginContainer}>
          <h1 className={styles.heading1}>Coditas</h1>
          <h1 className={styles.heading}>Sign in with Google-</h1>
          <button className={styles.submitBtn} onClick={() => login()}>Sign in with google</button>
        </div>
      </div >

      <div className={styles.sectionRight}>
        <span className={styles.colon}>;</span>
      </div>
    </section>
  )
}
export default Login