"use client";
import './globals.css';
import { SessionProvider } from "next-auth/react"
import AppContextProvider from '@/context/AppContext';
import { Router } from "next/router";
import { GoogleOAuthProvider } from '@react-oauth/google';

export default function RootLayout({
  children
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head />
      <body>
        <AppContextProvider>
          <GoogleOAuthProvider clientId="330679320155-vqo8g3275g3rpn99rsuhiu61thc2gu03.apps.googleusercontent.com">
            {children}
          </GoogleOAuthProvider>
        </AppContextProvider>
      </body>
    </html >
  )
}
