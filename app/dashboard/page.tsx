"use client";
import Header from '@/components/header'
import ShowForm from '@/components/showForm';
import { AppContext } from '@/context/AppContext';
import { getStore, setStore } from '@/utils/storeData';
import Image from 'next/image';
import React, { useContext, useEffect, useState } from 'react';
import styles from "./dashboard.module.scss";

const Dashboard = () => {
  const { router } = useContext(AppContext);
  const tempFormData: any = getStore("formdata");
  const tempFormDataArray: [] = JSON.parse(tempFormData);
  const [values, setValues] = useState<[any[]]>();

  const handleClick = (idx: number) => {
    const temp: any = [...tempFormDataArray];
    temp.splice(idx, 1);
    setStore("formdata", JSON.stringify(temp));
    setValues(temp)
  }

  useEffect(() => {
    const tempFormData: any = getStore("formdata");
    setValues(JSON.parse(tempFormData) || []);
  }, [tempFormDataArray.length])


  return (
    <>
      <Header />
      <>
        <>
          <button className={styles.button} onClick={() => router.push("/form")}>Form</button>
        </>
        <>
          <ShowForm
            data={values}
            handleClick={handleClick}
          />
        </>
      </>

    </>
  )
}

export default Dashboard