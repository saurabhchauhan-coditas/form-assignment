"use client"
import Header from '@/components/header';
import PersonelForm from '@/components/form';
import React, { useContext } from 'react';
import { AppContext } from '@/context/AppContext';
import { useRouter } from 'next/navigation';
import styles from "./HomePage.module.scss";

const HomePage = () => {
  const router = useRouter();
  return (
    <>
      <Header />
      <PersonelForm />
    </>
  )
}

export default HomePage