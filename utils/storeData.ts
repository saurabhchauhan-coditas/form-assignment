export const setStore = (key: string, payload: string) => {
  localStorage.setItem(key, payload);
}

export const getStore = (key: string) => {
  const data = localStorage.getItem(key);
  return data;
}

export const clearStore = (idx: number) => {
  const tempFormData: any = getStore("formdata");
  const tempFormDataArray: [] = JSON.parse(tempFormData);
  const temp = [...tempFormDataArray];
  temp.splice(idx, 1);
  setStore("formdata", JSON.stringify(temp));
}

export const clearAllStore = () => {
  localStorage.clear();
}