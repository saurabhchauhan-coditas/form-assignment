/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-useless-escape */
import { BaseSyntheticEvent, useMemo, useState } from 'react'
import { omit } from 'lodash'

const useFormValidation = () => {

  const specialCharRegex = /^[A-Za-z0-9 ]+$/;
  const passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;
  const emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const [values, setValues] = useState({});
  const [showError, setShowError] = useState<any>({});

  const handleFormChange = (event: BaseSyntheticEvent) => {
    const name = event.target.name;
    const val = event.target.value;
    setValues({
      ...values,
      [name]: val
    })

    switch (name) {
      case "firstName":
        if (val.length === 0) {
          setShowError({
            ...showError,
            firstName: "Enter Name"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            firstName: "Name must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "firstName");
          setShowError(newObj);
        }
        break;


      case "password":
        if (!new RegExp(passwordRegex).test(val)) {
          setShowError({
            ...showError,
            password: "Password must be 8 characters long,  one number, one special character and one uppercase letter."
          })
        }
        else {
          let newObj = omit(showError, "password");
          setShowError(newObj);
        }
        break;

      case "email":
        if (!new RegExp(emailRegex).test(val)) {
          setShowError(
            {
              ...showError,
              email: "Enter correct email format"
            })
        }
        else {
          let newObj = omit(showError, "email");
          setShowError(newObj);
        }
        break;

      case "mobileNumber":
        if (val.lenght === 0) {
          setShowError(
            {
              ...showError,
              mobile: "Enter correct mobile number format"
            })
        }
        else {
          let newObj = omit(showError, "mobileNumber");
          setShowError(newObj);
        }
        break;


      case "lastName":
        if (val.length === 0) {
          setShowError({
            ...showError,
            lastName: "Enter Name"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            lastName: "Name must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "lastName");
          setShowError(newObj);
        }
        break;

      case "country":
        if (val.length === 0) {
          setShowError({
            ...showError,
            country: "Enter Country"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            country: "Country must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "country");
          setShowError(newObj);
        }
        break;

      case "weight":
        if (val.length === 0) {
          setShowError({
            ...showError,
            weight: "Enter Weight"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            weight: "Weight must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "weight");
          setShowError(newObj);
        }
        break;

      case "height":
        if (val.length === 0) {
          setShowError({
            ...showError,
            height: "Enter Height"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            height: "Height must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "height");
          setShowError(newObj);
        }
        break;

      case "fatherName":
        if (val.length === 0) {
          setShowError({
            ...showError,
            fatherName: "Enter Name"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            fatherName: "Name must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "fatherName");
          setShowError(newObj);
        }
        break;

      case "motherName":
        if (val.length === 0) {
          setShowError({
            ...showError,
            motherName: "Enter Name"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            motherName: "Name must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "motherName");
          setShowError(newObj);
        }
        break;

      case "fatherCountry":
        if (val.length === 0) {
          setShowError({
            ...showError,
            fatherCountry: "Enter Country"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            fatherCountry: "Country must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "fatherCountry");
          setShowError(newObj);
        }
        break;

      case "motherCountry":
        if (val.length === 0) {
          setShowError({
            ...showError,
            motherCountry: "Enter Country"
          })
        }
        else if (!new RegExp(specialCharRegex).test(val)) {
          setShowError({
            ...showError,
            motherCountry: "Country must not have special characters"
          })
        }
        else {
          let newObj = omit(showError, "motherCountry");
          setShowError(newObj);
        }
        break;

      default: {
        break;
      }
    }
  }

  useMemo(() => handleFormChange, [values, showError])

  const handleFormSubmit = (event: BaseSyntheticEvent) => {
    event?.preventDefault();
    if (Object.keys(values).length === 0) {
      setShowError({
        ...showError,
        password: "Password Required",
        email: "Email Required",
        firstName: "Name Required",
        lastName: "Name Required",
        weight: "Weight Required",
        height: "Height Required",
        country: "Country Required",
        motherName: "Name Required",
        fatherName: "Name Required",
        motherCountry: "Country Required",
        fatherCountry: "Country Required"
      })
    }
  }

  return {
    handleFormChange,
    showError,
    handleFormSubmit,
  }
}

export default useFormValidation