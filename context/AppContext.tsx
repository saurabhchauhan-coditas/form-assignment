"use client"
import { createContext, useReducer, useState } from "react"
import { appReducer } from "../reducer/appReducer";
import { IAppContextProps } from "./AppContext.types";
import { useRouter } from 'next/navigation';

export const AppContext = createContext<any>({});

const AppContextProvider: React.FC<IAppContextProps> = (props) => {

  const initialState = {
    formData: []
  }

  const [appState, appDispatch] = useReducer<any>(appReducer, initialState);
  const router = useRouter();

  return (
    <AppContext.Provider value={{ appState, appDispatch, router }}>
      {props.children}
    </AppContext.Provider>
  )
}

export default AppContextProvider;
