import { clearStore, getStore, setStore } from "@/utils/storeData";

export interface IState {
  formData: [];
}

export type ActionType =
  | "setFormData"
  | "deleteFormData";

export interface IAction {
  type: ActionType;
  payload?: any;
}

export const appReducer = (state: IState, action: IAction) => {
  const { payload } = action;
  switch (action.type) {

    case "setFormData":
      setStore("formdata", JSON.stringify([...state.formData, payload]));
      return {
        ...state,
        formData: [...state.formData, payload]
      }
     


    case "deleteFormData":
      clearStore(payload);
      return;
      // const tempFormData: any = getStore("formdata");
      // const tempFormDataArray: [] = JSON.parse(tempFormData);
     
      // const filterData = tempFormDataArray.filter((ele: any, index: number) => index !== payload)

      // return {
      //   ...state,
      //   formData: filterData
      // }

    default:
      return state;
  }
};
